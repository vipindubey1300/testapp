
import React from 'react';
import MainNavigator from './src/navigators/MainNavigator';
import store from './src/store/store';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native'
import { LogBox } from 'react-native';


const App: () => React$Node = () => {

  LogBox.ignoreAllLogs()


  return (
    <Provider store={store}>
     <NavigationContainer>
       <MainNavigator/>
      </NavigationContainer>
    </Provider>
  );
};


export default App;
