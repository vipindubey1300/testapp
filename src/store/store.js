import { createStore,combineReducers,applyMiddleware } from "redux";
import listReducer from "../reducers/listReducer";
import apiReducer from "../reducers/apiReducer";
import thunk from 'redux-thunk';


const mainReducer = combineReducers({
    listReducer,
    apiReducer
  })

const store = createStore(mainReducer,applyMiddleware(thunk));


export default store;





