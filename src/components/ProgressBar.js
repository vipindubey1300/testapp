import React from 'react';
import { View,StyleSheet,ActivityIndicator} from 'react-native';


const ProgressBar = () => {
    return(
        <View style={[ StyleSheet.absoluteFill,
        { backgroundColor: 'rgba(0, 0, 0, 0.5)', justifyContent: 'center' } ]}>

          <ActivityIndicator size={50} color={'blue'} />
          
          </View>
    )
  };
  export default ProgressBar;





 