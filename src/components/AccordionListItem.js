import React, { useState, useRef,useEffect } from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  StyleSheet,
  Animated,
  Easing,Image
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';



const AccordionListItem = ({ title, children ,openDefault}) => {
  const [open, setOpen] = useState(false);
  const animatedController = useRef(new Animated.Value(0)).current;
  const [bodySectionHeight, setBodySectionHeight] = useState(0);

  useEffect(() => { 
   if(openDefault){
    Animated.timing(animatedController, {
        duration: 300,
        toValue: 1,
        easing: Easing.bezier(0.4, 0.0, 0.2, 1),
      }).start();
      setOpen(true)
   }

 }, []);

  const bodyHeight = animatedController.interpolate({
    inputRange: [0, 1],
    outputRange: [0, bodySectionHeight],
  });

  const arrowAngle = animatedController.interpolate({
    inputRange: [0, 1],
    outputRange: ['0rad', `${Math.PI}rad`],
  });

  const toggleListItem = () => {
    if (open) {
      Animated.timing(animatedController, {
        duration: 300,
        toValue: 0,
        easing: Easing.bezier(0.4, 0.0, 0.2, 1),
      }).start();
    } else {
      Animated.timing(animatedController, {
        duration: 300,
        toValue: 1,
        easing: Easing.bezier(0.4, 0.0, 0.2, 1),
      }).start();
    }
    setOpen(!open);
  };

  return (
    <View style={{marginVertical:8}}>
      <TouchableWithoutFeedback onPress={() => toggleListItem()}>
        <View style={[styles.titleContainer]}>
          <View style={{flexDirection:'row',alignItems:'center'}}>
            <Image source={{uri:'https://homepages.cae.wisc.edu/~ece533/images/airplane.png'}} style={styles.image}></Image>
            <Text style={{color:title.colorCode,fontWeight:'700',marginHorizontal:5}}> 
             {title.categoryName} {title.servingSize ? <Text style={{color:'black',fontWeight:'300'}}>({title.servingSize }) </Text> : ''}</Text>
          </View>
          <Animated.View style={{ transform: [{ rotateZ: arrowAngle }] }}>
            <Icon  name="caret-down" size={23} style={{marginHorizontal:5}} color="#373647"/>
          </Animated.View>
        </View>
      </TouchableWithoutFeedback>
      <Animated.View style={[styles.bodyBackground, { height: bodyHeight }]}>
        <View
          style={styles.bodyContainer}
          onLayout={event =>
            setBodySectionHeight(event.nativeEvent.layout.height)
          }>
          {children}
        </View>
      </Animated.View>

      {
          title.protip
         ? <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 0}} 
         colors={['#81a9e8', '#51aee1', '#8ae6e3']} 
         style={styles.gradientContainer}>
             <View style={styles.tipContainer}>
               <Text style={{color:'white'}}>TIP</Text>
             </View>
             <Text style={{width:'70%',color:'white'}}>{title.protip}</Text>
             </LinearGradient>
        : null      
     }
    </View>
  );
};
export default AccordionListItem;

const styles = StyleSheet.create({
  bodyBackground: {
    backgroundColor: 'white',
    overflow: 'hidden',
    marginHorizontal:10
  },
  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 3,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#EFEFEF',
    backgroundColor:'white',
    marginHorizontal:10
  },
  bodyContainer: {
    padding: 1,
    paddingLeft: 1.5,
    position: 'absolute',
    bottom: 0,
  },
  tipContainer:{paddingHorizontal:10,paddingVertical:5,borderRadius:15,backgroundColor:'#428ebf',width:60,justifyContent:'center',alignItems:'center',margin:5},
  image:{height:40,width:40,borderRadius:20,overflow:'hidden'},
  gradientContainer:{padding:10,borderRadius:10,width:'90%',alignSelf:'center',margin:10}
});
