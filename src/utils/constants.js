import {Dimensions} from 'react-native';



export const urls = {
  URL: 'https://api.jsonbin.io/b/5f2c36626f8e4e3faf2cb42e',
};


export const dimensions = {
  SCREEN_WIDTH : Dimensions.get('window').width,
  SCREEN_HEIGHT : Dimensions.get('window').height
};


