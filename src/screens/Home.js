import React from 'react'
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { dimensions } from '../utils/constants';


const Home : () => React$Node = (props) => {

  function _toProducts(){
    props.navigation.navigate("Products")
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
    
        <TouchableOpacity onPress={()=> _toProducts()}
        style={styles.button}>  
        <Text style={{color:'white'}}>Go to Products</Text>
       </TouchableOpacity>
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:'#e9e9f0',
      justifyContent:'center',
      alignItems:'center'
    },
    button:{
      backgroundColor:'blue',
      padding:10,
      borderRadius:15,
      width:dimensions.SCREEN_WIDTH * 0.8,
      alignItems:'center'
  }
   

})

export default Home;
