import React, { useState ,useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput, Alert
} from 'react-native';
import { useDispatch,useSelector } from 'react-redux';

import  {getData} from '../actions/actions';
import ProgressBar from '../components/ProgressBar';
import AccordionListItem from '../components/AccordionListItem';
import { dimensions } from '../utils/constants';
import Icon from 'react-native-vector-icons/FontAwesome';


const Products : () => React$Node = (props) => {

    const common = useSelector(state => state.apiReducer); //reducer name from store
    const listReducer = useSelector(state => state.listReducer);
    const dispatch = useDispatch();
    const [search, setSearch ] = useState('');
    const [list, setList ] = useState([]);


  const fetchData = async() =>{
       dispatch(getData())
       .then(response=> {
            setList(response.categories)
       })
       .catch(error => {
           console.log("Error !",error.message)
            Alert.alert(error.message)
       })
       

    }


    useEffect(() => { 
       if (listReducer.list.length == 0)  fetchData()
       else setList(listReducer.list)
    }, []);


   
    function _onSearch(value){
        setSearch(value)
        if(value.length == 0){
                //empty input 
                setList(listReducer.list)
        }
        else{
            var filtered = listReducer.list.filter(item => item.category.categoryName.includes(search));
            setList(filtered)
        }
        
    }
   

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
         <View style={[styles.rowContainer,styles.input]}>
         <Icon  name="search" size={20} style={{marginHorizontal:5}} color="#373647"/>
           <TextInput
             value = { search }
             placeholder={'Try searching fat,sauces names...'}
             placeholderTextColor={'grey'}
             onChangeText={(value) => { _onSearch(value)}}
           />
           </View>

         <ScrollView>
               
             {
                 list.map((item,index) =>(
                    <AccordionListItem openDefault={ false }
                     title={item.category}>
                     {item.category.subcategories.map(e => (
                         <View>
                             {
                                 e.subCategoryname ?  <Text 
                                 style={[{color:item.category.colorCode},
                                 styles.subTitle]}>{e.subCategoryname}</Text>
                                 : null
                             }
                        {
                            e.items.map(i => (
                               <View>
                                    <Text>{i}</Text>
                                    <View style={styles.divider}/>
                               </View>
                            ))
                        }
                         </View>
                     ))}
                     {
                         item.category.quote
                        ? <View style={{padding:10,backgroundColor:'#eef6fa',borderRadius:10,width:'90%',margin:10}}>
                            <Text>{item.category.quote}</Text>
                        </View>
                        : null
                     }
                    </AccordionListItem>
                 ))
             }

             
         </ScrollView>  
      </SafeAreaView>
         {common.loading && <ProgressBar/>}
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:'#e9e9f0'
    },
    subTitle:{
        fontWeight:'500',
        fontSize:16,
        marginVertical:6
    },
    divider:{
        height:0.6,
        width:dimensions.SCREEN_WIDTH,
        backgroundColor:'#D3D3D3',
        marginVertical:5
    },
    input:{
        height:45,
        width:'95%',
        alignSelf:'center',
        borderRadius:10,
        borderWidth:0.8,
        borderColor:'black',
        padding:8,
        marginVertical:15,
        marginHorizontal:4,
        backgroundColor:'#e7f0f6'
        
        
    },
    rowContainer:{
        flexDirection:'row',
        alignItems:'center'
    }
   

})

export default Products;
