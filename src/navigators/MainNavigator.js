import React from 'react';
import { createStackNavigator } from "@react-navigation/stack"

import Home from '../screens/Home.js';
import Products from '../screens/Products.js';


//navigators
const Stack = createStackNavigator()


const MainNavigator = () => {
     
  return (
      <Stack.Navigator initialRouteName="Home" mode='modal' options={{ gesturesEnabled: true,}}>
        <Stack.Screen name="Home" component={Home}   options={{headerShown:false}}/>
        <Stack.Screen name="Products" component={Products} />
      </Stack.Navigator>
  )
}

export default MainNavigator