import * as types from  "../reducers/types";
import { urls } from "../utils/constants";



export const getData = () => async dispatch => 
new Promise((resolve ,reject) => {
    dispatch({type: types.LOADING, isLoading:true });
    fetch(urls.URL, {method: 'GET'})
    .then((response) => response.json())
    .then( (responseJson) => {
        dispatch({ type: types.LOADING,isLoading:false });
        if(responseJson.categories.length > 0)  dispatch({ type: types.ADD_ITEMS,payload:responseJson.categories });
        resolve(responseJson);
    })
    .catch((error) => {
       dispatch({type: types.LOADING, isLoading:false});
        reject(error);
    });
  
})

  


export const fetchPosts = () => async dispatch => {
  dispatch({type: types.LOADING,  isLoading:false })
} 